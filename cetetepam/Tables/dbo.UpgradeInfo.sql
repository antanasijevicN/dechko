SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UpgradeInfo] (
		[Item]       [nvarchar](260) COLLATE Albanian_BIN NOT NULL,
		[Status]     [nvarchar](512) COLLATE Albanian_BIN NULL,
		CONSTRAINT [PK_UpgradeInfo]
		PRIMARY KEY
		CLUSTERED
		([Item])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UpgradeInfo] SET (LOCK_ESCALATION = TABLE)
GO
