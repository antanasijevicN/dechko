SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SecData] (
		[SecDataID]              [uniqueidentifier] NOT NULL,
		[PolicyID]               [uniqueidentifier] NOT NULL,
		[AuthType]               [int] NOT NULL,
		[XmlDescription]         [ntext] COLLATE Albanian_BIN NOT NULL,
		[NtSecDescPrimary]       [image] NOT NULL,
		[NtSecDescSecondary]     [ntext] COLLATE Albanian_BIN NULL,
		CONSTRAINT [PK_SecData]
		PRIMARY KEY
		NONCLUSTERED
		([SecDataID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_SecData]
	ON [dbo].[SecData] ([PolicyID], [AuthType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecData] SET (LOCK_ESCALATION = TABLE)
GO
